### Samin H. Farhangi, Ph.D.
📍 Cissy van Marxveldtstraat 4, 6708SJ, Wageningen, The Netherlands  
📞 +31 686 041 979  
📧 Samin.h.farhangi@gmail.com  

---

#### Research Interest
Bioinformatics & Computational Biology; Phylogenetics & Phylogenomics; Evolution & Molecular Dating; Molecular Biology

#### Education
- **M.Sc. Bioinformatics,** Wageningen University & Research, 2015-2017  
    - *Major thesis:* Detection of mutations in Pepino mosaic virus after horizontal transmission 
    - *Minor thesis:* Updating Pelargonium dating
    - *Supervisors:* Sandra Smit & Rene van der Vlugt (Major), Freek Bakker & Sara van de Kerke (Minor)

- **Ph.D. Plant Pathology,** University of Tehran, 2005-2010  
    - *Title:* Comparison of biological and molecular characterization of Iranian sunflower Tobacco streak virus isolate with Indian and Sudanese isolates by emphasis on 2b and CP genes
    - *Supervisory team:* Gholamhossein Mosahebi, Mina Koohi Habibi, Stephan Winter

- **M.Sc. Plant Pathology,** University of Tehran, 2001-2004  
    - *Title:* Characterization and distribution of Zucchini yellow mosaic virus in Tehran province
    - *Supervisory team:* Gholamhossein Mosahebi, Mina Koohi Habibi, Mahmod Okhovat

- **B.Sc. Plant Protection,** Vali-e-Asr University of Rafsanjan, 1997-2001

#### Professional Experience
- **Postdoctoral Researcher,** Wageningen University & Research; Animal Breeding and Genomics Group, 2021-2022  
    - *Project:* Identification of functional genetic variants in pigs for improved genomic selection
    - *Supervisor:* Prof. Martien Groenen

- **Teaching Assistant & Guest Researcher,** Wageningen University & Research; Biosystematics Group, 2018-2021  
    - *Courses:* Comparative biology and systematics, Advanced Biosystematics 

- **Data Analyst/Bioinformatician,** Urban Green Land Ltd., Kerman, Iran, 2015-2021  

- **Assistant Professor,** Vali-e-Asr University of Rafsanjan; Plant Pathology group, 2010-2015  
    - *Courses:* Plant pathology, Bioinformatics, Advanced plant virology, Molecular biology

- **Guest Researcher,** DSMZ Institute, Braunschweig, Germany, 2008-2009

#### Projects / Portfolio
- **[PPS-LIFT-1](https://gitlab.com/samin.h.farhangi/pps-lift-1):** Structured approach to RNA-seq data analysis...
- **[PPS-LIFT-2](https://gitlab.com/samin.h.farhangi/pps-lift-2):** Systematic procedure for eGWAS analysis...
- **[PPS-LIFT-3](https://gitlab.com/samin.h.farhangi/pps-lift-3):** Analyzing regulatory elements to identify variations...
- **[eQTL Annot](https://gitlab.com/samin.h.farhangi/eqtlannot):** Annotating and filtering the results of an eGWAS...
- **[VirHap](https://gitlab.com/samin.h.farhangi/virhap):** Integrated solution for bioinformatics analysis...

#### Selected Publications
- **2022,** *Structure and molecular evolution of the barcode fragment of cytochrome oxidase I (COI) in Macrocheles (Acari: Mesostigmata: Macrochelidae),* Ecology and Evolution
- **2021,** *Phylogenetic analysis of a Bean yellow mosaic virus isolate from Iran,* Physiological and Molecular Plant Pathology
- **2021,** *Microencapsulation of a Pseudomonas strain (VUPF506) in alginate–whey protein–carbon nanotubes,* Polymers
- **2020,** *LRR-RLKII phylogenetics reveals five main clades throughout the plant kingdom,* The Plant Journal
- **2019,** *Younger crown node and resolved phylogenetic issues in pelargonium,* Molecular Phylogenetics and Evolution

#### Patents
- **2021,** LRR-RLKII receptor kinase interaction domains, WO2021015616A1

#### Skills
- **Programming:** Python, R
- **Operating Systems:** Linux command line and servers
- **Analysis:** NGS data, GWAS and eGWAS, RNASeq, Phylogenetics, Molecular dating
- **Lab Skills:** RNA/DNA extraction, cloning, electrophoresis, western blot, ELISA, PCR
- **Languages:** Farsi (Native), English (Advanced), Dutch (Pre-intermediate)
